                                                   List of databases
       Name        |       Owner       | Encoding |   Collate   |    Ctype    |            Access privileges            
-------------------+-------------------+----------+-------------+-------------+-----------------------------------------
 books             | diakurniadewi     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 bookstore         | dia               | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 codegig           | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 demo              | diakurniadewi     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 dev_database_url  | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 firstDB           | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 itasset           | diakurniadewi     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =Tc/diakurniadewi                      +
                   |                   |          |             |             | diakurniadewi=CTc/diakurniadewi
 junglephasedb     | bejunglephase     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =Tc/bejunglephase                      +
                   |                   |          |             |             | bejunglephase=CTc/bejunglephase
 junglephasedbtest | bejunglephasetest | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =Tc/bejunglephasetest                  +
                   |                   |          |             |             | bejunglephasetest=CTc/bejunglephasetest
 latihan           | dia               | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 latihan2          | dia               | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 latihantest       | dia               | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 mydb              | diah              | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 node_sequelize    | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 postgres          | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 relation_sql_demo | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 sequelize1        | dia               | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =Tc/dia                                +
                   |                   |          |             |             | dia=CTc/dia
 sequelize_assoc   | dia               | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 smartattendance   | dia               | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 template0         | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres                            +
                   |                   |          |             |             | postgres=CTc/postgres
 template1         | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres                            +
                   |                   |          |             |             | postgres=CTc/postgres
 test              | diakurniadewi     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 test_database_url | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 trial2            | dia               | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 trial3            | dia               | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 trial4            | postgres          | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
(26 rows)

